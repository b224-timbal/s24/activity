//Template Literals using exponent operator
let num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

// Destructuring Array
const address = ["Candijay", "Bohol", "Philippines"];
const [city, province, country] = address;
console.log(`I live at ${city}, ${province}, ${country}.`);

//Destructuring Object
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	heightFt: 20,
	heightIn: 3 
};
const {name, type, weight, heightFt, heightIn} = animal;
function getAnimal({name, type, weight, heightFt, heightIn}) {
	console.log(`${name} was a ${type}. He is weighed at ${weight} kgs with a measurement of ${heightFt} ft ${heightIn} in.`);
};

getAnimal(animal);

//Loop
const numbers = [1, 2, 3, 4, 5, 15];
numbers.forEach((number) => {
	const printNumber = (number) => number;
	console.log(printNumber(number));
});

// Dog Class
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);